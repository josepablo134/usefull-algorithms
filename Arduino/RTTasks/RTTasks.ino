///	Tasks prototypes	{
#define Tester			13
#define controlTime	1
void controlSetup();
void controlLoop();
#define measTime	1
void measSetup();
void measLoop();
///	Tasks prototypes	}

/**
	Scheduler configuration
*/
///	Scheduler setup
void SchedulerSetup();
///	Scheduler increment tick if it is needed
void SchedulerTick();
///	Task format
typedef void (*taskFxn)(void);
///	Tick format
typedef uint16_t	Tick;
///	System Tick Counter
Tick SysTick = 0;

/**
	Task registration
*/
#define maxTasks	2
taskFxn tasksSetups[] = {controlSetup,measSetup};
taskFxn tasksLoops[] = {controlLoop,measLoop};
Tick		tasksTime[] = {controlTime,measTime};
Tick		tasksPeriod[] = {5,5};

/**
	System entry point
*/
void setup() {
	SchedulerSetup();
}
/**
	Dispatcher loop
*/
void loop() {
	SchedulerTick();
}


unsigned long SchedulerTime = 0;
void SchedulerSetup(){
	///	Capture system time
	SchedulerTime = millis();
	///	Setup all tasks
	for(register uint8_t counter;counter<maxTasks;counter++){
		tasksSetups[counter]();
	}
}

void SchedulerTick(){
	static unsigned long localSchedulerTime;
	localSchedulerTime = millis();
	///	Non real real-time!
	if( localSchedulerTime != SchedulerTime ){
		SchedulerTime = localSchedulerTime;
		SysTick += 1;
			for(register uint8_t counter=0;counter<maxTasks;counter++){
				if( tasksTime[counter] == SysTick ){
					tasksLoops[counter]();
					tasksTime[counter] += tasksPeriod[counter];
				}
			}
	}
}

#define FB      		A0
#define AIN     		A1
#define	maxVoltage		5
#define	maxADCVal		1024
///	Shared variables
float				setpoint,feedback;///Volts
void measSetup(){
	///	Nothing to configure
	pinMode( Tester , OUTPUT );
}
void measLoop(){
	const float factor = ((float)maxVoltage/(float)maxADCVal);
	///	Setpoint is direct
	setpoint = ((float)( analogRead( AIN ) )) * factor;
	///	Feedback is inverted
	feedback = ((float)(1023-analogRead( FB ))) * factor;
	//digitalWrite( Tester , !digitalRead(Tester) );
}

#define PWMOUT  		3
#define	PWMFactor		0.25///ADCVoltage to PWM signal
#define Kp				0.05
void controlSetup(){
	pinMode(PWMOUT,OUTPUT);
	/// Set timer 2 preescaler 31373.55 Hz
	TCCR2B = TCCR2B & 0b11111000 | 0x01;
}
void controlLoop(){
	const	float			factor = (maxADCVal/maxVoltage)*PWMFactor;
	static float 		error;
	static uint8_t	output;
	static float		p;
	error = setpoint - feedback;
	p = error*Kp;
	if( p<0.0 ){
		p=0;
	}else
	if( p> 5.0 ){
		p = 5.0;
	}
	p = p*(factor);
	output = (uint8_t)p;
	analogWrite( PWMOUT , output );
	//digitalWrite( Tester , !digitalRead(Tester) );
}

