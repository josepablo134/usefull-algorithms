/*!
*	@file		BLAS.c
*	@version	1.0
*	@author		Josepablo Cruz Baas
*	@date		04-29-2019
*	@title		BLAS ( Basic Linear Algebra Subprograms )
*	BLAS Implementation
**/
#include "BLAS.h"
#include <math.h>

#ifdef BLAS_MALLOC_AVAILABLE
/*!
*	@sa	array( size_t len )
*	@brief	allocate memory for the vector
*	@param	len	size_t type for the vector length
*	@return	blas_vector	a vector of blas_var elements or NULL if an error has ocurred
**/
	blas_var*	array( size_t len ){
		return (blas_var*) malloc( len*sizeof( blas_var ) );
	}
/*!
*	@sa		ones( size_t len )
*	@brief	allocate memory for the vector and initialize its content with ones
*	@param	len	size_t type for the vector length
*	@return	blas_vector a vector of blas_var elements or NULL if an error has occurred
**/
	blas_var*	ones( size_t len ){
		register size_t k;
		blas_var*	temp = array( len );
		if(! temp ){ return 0x00; }
		for(k=0;k<len;k++){
			temp[k] = 1.0;
		}
		return temp;
	}
/*!
*	@sa		zeros( size_t len )
*	@brief	allocate memory for the vector and initialize its content with zeros
*	@param	len size_t type for the vector length
*	@return	blas_vector	a vector of blas_var elements or NULL if an error has occurred
**/
	blas_var*	zeros( size_t len ){
		register size_t k;
		blas_var*	temp = array( len );
		if(! temp ){ return 0x00; }
		for(k=0;k<len;k++){
			temp[k] = 0.0;
		}
		return temp;
	}
/*!
*	@sa		afree( blas_vector );
*	@brief	free memory allocated with any of the interfaces for blas_vector
*	@param	vector	a blas_vector type to be free
**/
	void		afree( blas_var* array){
		if(! array ){ return; }
		free(array);
	}
#endif

///	Non struct data
	/*!
	*	@sa	set( value , vector , len )
	*	@brief	Set a value to all elements in a vector
	*	@param	value a blas_var value to set in the vector
	*	@param	vector a blas_vector where to set the value
	*	@param	len	length of the destination vector
	**/
void aset( const blas_var value, blas_var* vector, size_t len ){
	register size_t k;
	if(! vector ){ return; }
	for(k=0;k<len;k++){
		vector[k] = value;
	}
}
	/*!
	*	@sa	copy( vectorDes , vectorSource , len )
	*	@brief	copy all elements in the source vector to the destination vector
	*	@param	vectorDes, a blas_vector
	*	@param	vectorSource, a blas_vector
	*	@param	len, a size_t type
	**/
void acopy( blas_var* vectorDes, blas_var* vectorSource, size_t len ){
	register size_t	k;
	if( !vectorDes || !vectorSource){ return; }
	for(k=0;k<len;k++){
		vectorDes[k] = vectorSource[k];
	}
}
	/*!
	*	@sa	swap( vectorA , vectorB , len )
	*	@brief	swap values between vectorA and vectorB
	*	@param	vectorA, a blas_vector
	*	@param	vectorB, a blas_vector
	*	@param	len, a size_t type, length of the smaller vector
	**/
void aswap( blas_var* vectorA, blas_var* vectorB, size_t len ){
	register size_t	k;
	blas_var		temp;
	if(!vectorA || !vectorB){ return; }
	for(k=0;k<len;k++){
		temp = vectorB[k];
		vectorB[k] = vectorA[k];
		vectorA[k] = temp;
	}
}
	/*!
	*	@sa	adot( vectorA , vectorB , len )
	*	@brief	dot operation over vectorA and vectorB
	*	@param	vectorA, a blas_vector
	*	@param	vectorB, a blas_vector
	*	@param	len, a size_t type, lenght of the smaller vector
	*	@return	blas_var, result of the operation
	**/
blas_var adot( blas_var* vectorA, blas_var* vectorB, size_t len ){
	register size_t k;
	blas_var		temp=0.0;
	if(!vectorA || !vectorB){ return -0.0; }
	for(k=0;k<len;k++){
		temp += vectorA[k]*vectorB[k];
	}
	return temp;
}
	/*!
	*	@sa	anormalize( vector , vectorDest , len )
	*	@brief	normalize the value of all elements in the vector in a range from 0 to 1
	*	@param	vector, a blas_vector
	*	@param	vectorDest, a blas_vector where to save the result
	*	@param	len, a size_t type length of the vector
	**/
void anormalize( blas_var* vector, blas_var* vectorDest, size_t len ){
	register size_t k;
	blas_var 		min,max;
	if(!vector){ return; }
	if(!vectorDest){
		vectorDest = vector;
	}
	min = amin( vector , len );
	max = amax( vector , len ) - min;
	if(! max ){
		return aset( 1.0 ,  vector , len );
	}
	for(k=0;k<len;k++){
		vectorDest[k] -= min;
		vectorDest[k] /= max; 
	}
}
	/*!
	*	@sa	anorm( vector , len )
	*	@brief	evaluate the norm of the vector
	*	@param	vector, a blas_vector type
	*	@param	len, a size_t type, length of the vector
	*	@return	a blas_var type, result of the operation
	**/
blas_var anorm( blas_var* vector, size_t len){
	register size_t k;
	blas_var		temp=0;
	if(!vector){return -0.0;}
	for(k=0;k<len;k++){
		temp += vector[k]*vector[k];
	}
	return sqrt(temp);
}
	/*!
	*	@sa	asumA( vector , len )
	*	@brief	sum all elements of the vector
	*	@param	vector, a blas_vector type
	*	@param	len, a size_t type
	*	@return a blas_var type, result of the operation
	**/
blas_var asumA( blas_var* vector, size_t len ){
	register size_t	k;
	blas_var		temp=0.0;
	if( !vector ){ return -0.0; }
	for(k=0;k<len;k++){
		temp += vector[k];
	}
	return temp;
}
	/*!
	*	@sa	asum( vectorA , vectorB , vectorC , len )
	*	@brief	sum all elements in vector A and B and save it in C
	*		if vectorC is NULL, vectorA is the vectorC
	*	@param	vectorA, a blas_vector type operator
	*	@param	vectorB, a blas_vector type operator
	*	@param	vectorC, a blas_vector type operator to save the result
	*	@param	len, a size_t type, length of the smaller vector
	**/
void asum( blas_var* vectorA , blas_var* vectorB , blas_var* vectorC , size_t len){
	register size_t k;
	if( !vectorA || !vectorB ){
		return;
	}
	if( !vectorC ){ vectorC = vectorA; }
	for(k=0;k<len;k++){
		vectorC[k] = vectorA[k] + vectorB[k];
	}
	return;
}
	/*!
	*	@sa	asub( vectorA , vectorB , vectorC , len )
	*	@brief substraction of vectorB from vectorA, store result in vectorC
	*		if vectorC is NULL, vectorA is vectorC
	*	@param	vectorA, a blas_vector type
	*	@param	vectorB, a blas_vector type
	*	@param	vectorC, a blas_vector type
	*	@param	len, a size_t type, length of the smaller vector
	**/
void asub( blas_var* vectorA , blas_var* vectorB , blas_var* vectorC , size_t len){
	register size_t k;
	if( !vectorA || !vectorB ){
		return;
	}
	if( !vectorC ){ vectorC = vectorA; }
	for(k=0;k<len;k++){
		vectorC[k] = vectorA[k] - vectorB[k];
	}
	return;
}
	/*!
	*	@sa	ascalar_mul( value , vector , vectorDest , len )
	*	@brief	multiply a scalar with vector and store its result in vectorDest,
	*		if vectorDest is NULL, vector is the vectorDest
	*	@param	value, a blas_var type, scalar
	*	@param	vector, a blas_vector type
	*	@param	vectorDest, a blas_vector type
	*	@param	len, a size_t type, length of the smaller vector
	**/
void ascalar_mul( const blas_var value, blas_var* vector , blas_var* vectorDest , size_t len){
	register size_t k;
	if( !vector ){return;}
	if( !vectorDest ){
		vectorDest = vector;
	}
	for(k=0;k<len;k++){
		vectorDest[k] *= value;
	}
}
	/*!
	*	@sa	ascalar_div( value , vector , vectorDest , len )
	*	@brief	multiply the inverse scalar with vector and store its result in vectorDest,
	*		if vectorDest is NULL, vector is the vectorDest
	*	@param	value, a blas_var type, scalar
	*	@param	vector, a blas_vector type
	*	@param	vectorDest, a blas_vector type
	*	@param	len, a size_t type, length of the smaller vector
	**/
void ascalar_div( const blas_var value, blas_var* vector , blas_var* vectorDest , size_t len){
	register size_t k;
	if( !vector ){return;}
	if( !vectorDest ){
		vectorDest = vector;
	}
	for(k=0;k<len;k++){
		vectorDest[k] /= value;
	}
}
	/*!
	*	@sa	amin( vector , len );
	*	@brief	find the minimum value in the vector
	*	@param vector, a blas_vector
	*	@param len, a size_t type, length of the vector
	*	@return blas_var type, the minimum value
	**/
blas_var amin( blas_var* vector , size_t len ){
	register size_t	k;
	blas_var		temp;
	if( !vector ){ return -0.0; }
	temp = vector[0];
	for(k=1;k<len;k++){
		if(temp>vector[k]){
			temp = vector[k];
		}
	}
	return temp;
}
	/*!
	*	@sa	amax( vector , len )
	*	@brief	find the maximum value in the vector
	*	@param	vector, a blas_vector
	*	@param	len, a size_t type, length of the vector
	*	@return blas_var type, the maximum value
	**/
blas_var amax( blas_var* vector , size_t len ){
	register size_t	k;
	blas_var		temp;
	if( !vector ){ return -0.0; }
	temp = vector[0];
	for(k=1;k<len;k++){
		if(temp<vector[k]){
			temp = vector[k];
		}
	}
	return temp;
}
	/*!
	*	@sa	argmax( vector , len )
	*	@brief	find the index of the maximum element in the vector
	*	@param	vector, a blas_vector
	*	@param	len, a size_t type, the length of the vector
	*	@return size_t type, index element of the maximum element
	**/
size_t argmax( blas_var* vector , size_t len ){
	register size_t	k;
	size_t			index=0;
	blas_var		temp;
	if( !vector ){ return -0.0; }
	temp = vector[0];
	for(k=1;k<len;k++){
		if(temp<vector[k]){
			temp = vector[k];
			index = k;
		}
	}
	return index;
}
	/*!
	*	@sa	argmin( vector , len )
	*	@brief	find the index of the minimum element in the vector
	*	@param	vector, a blas_vector
	*	@param	len, a size_t type, the length of the vector
	*	@return size_t type, index element of the minimum element
	**/
size_t argmin( blas_var* vector , size_t len ){
	register size_t	k;
	size_t			index=0;
	blas_var		temp;
	if( !vector ){ return -0.0; }
	temp = vector[0];
	for(k=1;k<len;k++){
		if(temp>vector[k]){
			temp = vector[k];
			index = k;
		}
	}
	return index;
}
	/*!
	*	@sa	amean( vector , len )
	*	@brief	compute the mean of the elements in the vector
	*	@param	vector, a blas_vector
	*	@param	len, a size_t type, length of the vector
	*	@return blas_var, the mean value
	**/
blas_var amean( blas_var* vector, size_t len ){
	blas_var		temp;
	if(!vector){return -0.0;}
	temp = asumA( vector , len );
	temp /= (blas_var)len;
	return temp;
}
	/*!
	*	@sa	avar( vector , len )
	*	@brief	compute the variance of the elements in the vector
	*	@param	vector, a blas_vector type
	*	@param	len, a size_t type, length of the vector
	*	@return blas_var, variance value
	**/
blas_var avar( blas_var* vector, size_t len ){
	register size_t	k;
	blas_var		temp,sum,mean;
	if(!vector){return -0.0;}
	temp = sum = 0.0;
	mean = amean( vector, len );
	for(k=0;k<len;k++){
		temp = vector[k]-mean;
		sum += temp*temp;
	}
	return (sum/ (blas_var) len);
}
	/*!
	*	@sa	astd( vector , len )
	*	@brief	compute the standard deviation of the elements of the vector
	*	@param	vector, blas_vector type
	*	@param	len, a size_t type, length of the vector
	*	@return	blas_var type, standard deviation value
	**/
blas_var astd( blas_var* vector, size_t len ){
	blas_var temp = avar( vector , len );
	return sqrt(temp);
}

