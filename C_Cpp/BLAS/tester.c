/*!
*	A test code, implementation and usage of the BLAS library
**/
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define BLAS_MALLOC_AVAILABLE	1
#include "BLAS.h"

void printArray( blas_var* , size_t );

int arrayTest();
int vectorTest();
int matrixTest();

size_t	vectorLen=0;
typedef blas_var	var;
int main( int argc , char** argv ){
	if( argc<2 ){
		printf( "%s [vector len]\n" , argv[0] );
		exit(-1);
	}
	vectorLen = atol( argv[1] );
	if( vectorLen == '\0' ){
		printf( "%s , is not valid\n" , argv[0] );
		return -1;
	}
	srand(time(NULL));
	return arrayTest();
}

void printArray( blas_var* vector, size_t len ){
	register size_t k;
	if( !vector ){return;}
	for(k=0;k<len;k++){
		printf( "%f, ",vector[k] );
	}
	printf( "\n" );
}

int arrayTest(){
	size_t	k;
	var		*vectorA = array( vectorLen );
	var		*vectorB = ones( vectorLen );
	var		*vectorC = zeros( vectorLen );
	printf("vector A\n");
	printArray( vectorA , vectorLen );
	printf("vector B\n");
	printArray( vectorB , vectorLen );
	printf("vector C\n");
	printArray( vectorC , vectorLen );

	asum( vectorB , vectorC , vectorA , vectorLen );
	ascalar_mul( 3.1416 , vectorB , 0x00 , vectorLen);
	ascalar_div( 0.5 , vectorA , vectorA , vectorLen );
	asub( vectorB , vectorA , vectorC , vectorLen );
	
	for(k=0;k<vectorLen;k++){
		vectorB[k] = (var)(rand()%100) / 10.0;
	}
	printf("vector A, (1/0.5)(vectorB+vectorC)\n");
	printArray( vectorA , vectorLen );
	printf("vector C, (3.1416*vectorB)-vectorA\n");
	printArray( vectorC , vectorLen );

	printf("vector B\n");
	printArray( vectorB , vectorLen );
	printf( "vector B max : %f\n", amax( vectorB , vectorLen) );
	printf( "vector B ArgMax : %ld\n", argmax( vectorB , vectorLen) );
	printf( "vector B min : %f\n", amin( vectorB , vectorLen) );
	printf( "vector B ArgMin : %ld\n", argmin( vectorB , vectorLen) );
	printf( "vector B mean : %f\n", amean( vectorB , vectorLen) );
	printf( "vector B var : %f\n", avar( vectorB , vectorLen) );
	printf( "vector B std : %f\n", sqrt(avar( vectorB , vectorLen) ) );
	printf( "vector B norm: %f\n", anorm( vectorB , vectorLen) );
	printf( "vector B sum: %f\n", asumA( vectorB , vectorLen) );
	printf( "vector B*A: %f\n", adot( vectorB , vectorA , vectorLen) );
	printf( "vector B normalized\n");
	anormalize( vectorB , 0x00, vectorLen );
	printArray( vectorB , vectorLen );

	printf("Setting 0.0 to vectorC\n");
	printf("Swapping vectorC and vectorB\n");
	aset( 0.0 , vectorC , vectorLen );
	aswap( vectorB , vectorC , vectorLen );
	printf("vector B\n");
	printArray( vectorB , vectorLen );
	printf("vector C\n");
	printArray( vectorC , vectorLen );

	printf("Array Test PASSED!\n");
	afree( vectorA );
	afree( vectorB );
	afree( vectorC );
	return 0;
}
