/*!
*	@file		BLAS.h
*	@version	1.0
*	@author		Josepablo Cruz Baas
*	@date		04-29-2019
*	@title		BLAS ( Basic Linear Algebra Subprograms )
*	BLAS header file - API description
*	@code
*		typedef blas_var	var;
*		typedef blas_vector vector;
*		vector	A = ones( 30 );
*		vector	B = array ( 30 );
*		var		d,m,M,s,o;
*		aset( 3.1416 , B , 30 );
*		d = adot( A , B , 30 );
*		m = amean( B , 30 );
*		/// and so on . . . 
*	@endcode
**/
#ifndef BLAS_H_
#define BLAS_H_
	#include <stdlib.h>
	///	BASIC LINEAR ALGEBRA ROUTINES
	typedef	float		blas_var;
	typedef blas_var*	blas_vector;

	#define BLAS_MALLOC_AVAILABLE	1
	#ifdef BLAS_MALLOC_AVAILABLE
	/// If there is malloc availble in the system
		/*!
		*	@sa	array( size_t len )
		*	@brief	allocate memory for the vector
		*	@param	len	size_t type for the vector length
		*	@return	blas_vector	a vector of blas_var elements or NULL if an error has ocurred
		**/
		extern blas_var*	array( size_t );
		/*!
		*	@sa		ones( size_t len )
		*	@brief	allocate memory for the vector and initialize its content with ones
		*	@param	len	size_t type for the vector length
		*	@return	blas_vector a vector of blas_var elements or NULL if an error has occurred
		**/
		extern blas_var*	ones( size_t );
		/*!
		*	@sa		zeros( size_t len )
		*	@brief	allocate memory for the vector and initialize its content with zeros
		*	@param	len size_t type for the vector length
		*	@return	blas_vector	a vector of blas_var elements or NULL if an error has occurred
		**/
		extern blas_var*	zeros( size_t );
		/*!
		*	@sa		afree( blas_vector );
		*	@brief	free memory allocated with any of the interfaces for blas_vector
		*	@param	vector	a blas_vector type to be free
		**/
		extern void			afree( blas_var* );
	#endif

	///	Non struct data
	/*!
	*	@sa	set( value , vector , len )
	*	@brief	Set a value to all elements in a vector
	*	@param	value a blas_var value to set in the vector
	*	@param	vector a blas_vector where to set the value
	*	@param	len	length of the destination vector
	*	@return
	**/
	extern void aset( const blas_var , blas_var* , size_t );
	/*!
	*	@sa	copy( vectorDes , vectorSource , len )
	*	@brief	copy all elements in the source vector to the destination vector
	*	@param	vectorDes, a blas_vector
	*	@param	vectorSource, a blas_vector
	*	@param	len, a size_t type
	**/
	extern void acopy( blas_var* , blas_var* , size_t );
	/*!
	*	@sa	swap( vectorA , vectorB , len )
	*	@brief	swap values between vectorA and vectorB
	*	@param	vectorA, a blas_vector
	*	@param	vectorB, a blas_vector
	*	@param	len, a size_t type, length of the smaller vector
	**/
	extern void aswap( blas_var* , blas_var* , size_t );
	/*!
	*	@sa	adot( vectorA , vectorB , len )
	*	@brief	dot operation over vectorA and vectorB
	*	@param	vectorA, a blas_vector
	*	@param	vectorB, a blas_vector
	*	@param	len, a size_t type, lenght of the smaller vector
	*	@return	blas_var, result of the operation
	**/
	extern blas_var adot( blas_var* , blas_var* , size_t );
	/*!
	*	@sa	anormalize( vector , vectorDest , len )
	*	@brief	normalize the value of all elements in the vector in a range from 0 to 1
	*	@param	vector, a blas_vector
	*	@param	vectorDest, a blas_vector where to save the result
	*	@param	len, a size_t type length of the vector
	**/
	extern void anormalize( blas_var*, blas_var* , size_t );
	/*!
	*	@sa	anorm( vector , len )
	*	@brief	evaluate the norm of the vector
	*	@param	vector, a blas_vector type
	*	@param	len, a size_t type, length of the vector
	*	@return	a blas_var type, result of the operation
	**/
	extern blas_var anorm( blas_var* , size_t );
	/*!
	*	@sa	asumA( vector , len )
	*	@brief	sum all elements of the vector
	*	@param	vector, a blas_vector type
	*	@param	len, a size_t type
	*	@return a blas_var type, result of the operation
	**/
	extern blas_var asumA( blas_var* , size_t );
	/*!
	*	@sa	asum( vectorA , vectorB , vectorC , len )
	*	@brief	sum all elements in vector A and B and save it in C
	*		if vectorC is NULL, vectorA is the vectorC
	*	@param	vectorA, a blas_vector type operator
	*	@param	vectorB, a blas_vector type operator
	*	@param	vectorC, a blas_vector type operator to save the result
	*	@param	len, a size_t type, length of the smaller vector
	**/
	extern void asum( blas_var* , blas_var*, blas_var*, size_t );
	/*!
	*	@sa	asub( vectorA , vectorB , vectorC , len )
	*	@brief substraction of vectorB from vectorA, store result in vectorC
	*		if vectorC is NULL, vectorA is vectorC
	*	@param	vectorA, a blas_vector type
	*	@param	vectorB, a blas_vector type
	*	@param	vectorC, a blas_vector type
	*	@param	len, a size_t type, length of the smaller vector
	**/
	extern void asub( blas_var* , blas_var*, blas_var*, size_t );
	/*!
	*	@sa	ascalar_mul( value , vector , vectorDest , len )
	*	@brief	multiply a scalar with vector and store its result in vectorDest,
	*		if vectorDest is NULL, vector is the vectorDest
	*	@param	value, a blas_var type, scalar
	*	@param	vector, a blas_vector type
	*	@param	vectorDest, a blas_vector type
	*	@param	len, a size_t type, length of the smaller vector
	**/
	extern void ascalar_mul( const blas_var, blas_var*, blas_var*, size_t len);
	/*!
	*	@sa	ascalar_div( value , vector , vectorDest , len )
	*	@brief	multiply the inverse scalar with vector and store its result in vectorDest,
	*		if vectorDest is NULL, vector is the vectorDest
	*	@param	value, a blas_var type, scalar
	*	@param	vector, a blas_vector type
	*	@param	vectorDest, a blas_vector type
	*	@param	len, a size_t type, length of the smaller vector
	**/
	extern void ascalar_div( const blas_var, blas_var*, blas_var*, size_t len);
	/*!
	*	@sa	amin( vector , len );
	*	@brief	find the minimum value in the vector
	*	@param vector, a blas_vector
	*	@param len, a size_t type, length of the vector
	*	@return blas_var type, the minimum value
	**/
	extern blas_var amin( blas_var* , size_t );
	/*!
	*	@sa	amax( vector , len )
	*	@brief	find the maximum value in the vector
	*	@param	vector, a blas_vector
	*	@param	len, a size_t type, length of the vector
	*	@return blas_var type, the maximum value
	**/
	extern blas_var amax( blas_var* , size_t );
	/*!
	*	@sa	argmax( vector , len )
	*	@brief	find the index of the maximum element in the vector
	*	@param	vector, a blas_vector
	*	@param	len, a size_t type, the length of the vector
	*	@return size_t type, index element of the maximum element
	**/
	extern size_t argmax( blas_var* , size_t );
	/*!
	*	@sa	argmin( vector , len )
	*	@brief	find the index of the minimum element in the vector
	*	@param	vector, a blas_vector
	*	@param	len, a size_t type, the length of the vector
	*	@return size_t type, index element of the minimum element
	**/
	extern size_t argmin( blas_var* , size_t );
	/*!
	*	@sa	amean( vector , len )
	*	@brief	compute the mean of the elements in the vector
	*	@param	vector, a blas_vector
	*	@param	len, a size_t type, length of the vector
	*	@return blas_var, the mean value
	**/
	extern blas_var amean( blas_var* , size_t );
	/*!
	*	@sa	avar( vector , len )
	*	@brief	compute the variance of the elements in the vector
	*	@param	vector, a blas_vector type
	*	@param	len, a size_t type, length of the vector
	*	@return blas_var, variance value
	**/
	extern blas_var avar( blas_var* , size_t );
	/*!
	*	@sa	astd( vector , len )
	*	@brief	compute the standard deviation of the elements of the vector
	*	@param	vector, blas_vector type
	*	@param	len, a size_t type, length of the vector
	*	@return	blas_var type, standard deviation value
	**/
	extern blas_var astd( blas_var* , size_t );
#endif
