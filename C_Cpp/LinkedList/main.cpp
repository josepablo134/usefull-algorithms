/*
*
*	Josepablo Cruz Baas
*	
*
**/
#include <iostream>
#include "LinkedList.hpp"

using namespace compsci;
int main(int argc, char** argv , char** argp){
	LinkedList<int>		list;
					///8Gb
	#define		MAX_ELEMENTS	134217728//1<<20
	for(int i=0; i<MAX_ELEMENTS ; i++){
		list.append(i+1);/// Agregar al final
		//list.push( i );/// Agregar al inicio
	}
/*
	Node<int>		*temp = list.find(-10);
	if(temp){
		std::cout<< temp->get() << std::endl;	
	}else{
		std::cout<< "no result" << std::endl;
	}
*/
	//Node<int>		*temp = list.getBeginNode();///Obtener el primer nodo
	//Node<int>		*temp = list.getEndNode();///Obtener el ultimo nodo
	using namespace std;
	cout<<"Press any key to continue..."<<endl;
	cin.get();
	int	temp=0;

	//cout<<"List nodes left before : " <<list.remove( -2 )<<endl;
	//cout<<"List nodes left before : " <<list.remove( -2 )<<endl;
	for( int i=0; i<=MAX_ELEMENTS ; i++ ){
		list.pop( &temp , -1 );
		//cout<<"List nodes left : "<<list.pop(&temp,-1)<<endl;
		//cout<<temp<<endl;	
	}
	cout<<"All memory erased"<<endl;

/*	
	for(int i=0; i<=MAX_ELEMENTS ; i++){
		cout<<"finding : "<< -1*i <<endl;
		if( list.get( &temp , -1*i ) < 0 ){
			cout<<"Index error"<<endl;
		}else{
			cout<< temp <<endl;
		}
	}
*/

	//cout<<"Linked List Test"<<endl;
	//while(temp){
		//cout<< temp->get() <<endl;
		//temp = temp->getLast();	
		//temp = temp->getNext();
	//}
	cout<<"Press any key..."<<endl;
	cin.get();
	return 0;
}


/*
///
//	Node testing
//
using namespace compsci;
int main(int argc , char** argv , char** argp){
	Node<int>	element[10];
	Node<int>*	temp;
	for(int i=1;i<9;i++){
		element[i].set(i);
		element[i].setNext( &element[i+1] );
		element[i].setLast( &element[i-1] );
	}
	element[0].set(0);
	element[0].setNext( &(element[1]) );
	element[9].set(9);
	element[9].setLast( &(element[8]) );
	
	using namespace std;
	cout<<"Node Test"<<endl;
	temp = element;
	try{
		while(temp){
			cout<<temp->get()<<endl;
			temp= temp->getNext();	
		}
	}catch( exception e){
		cerr<<"Oh oh"<<endl;
		return -1;
	}
	return 0;
}
*/
