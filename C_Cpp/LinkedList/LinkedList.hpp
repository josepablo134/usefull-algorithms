#ifndef	_LINKED_LSIT_HPP_
#define	_LINKED_LIST_HPP_
namespace compsci{
	typedef	signed long int	llsize_t;
	template <class T>
	class	Node{
	private:
		T		*data;
		Node<T>		*next,*last;
	public:
		Node(void);//checked
		~Node(void);//checked
		T	get(void);//checked
		void	set(T data);//checked
		Node<T>*	setNext(Node<T>* node);//checked
		Node<T>*	setLast(Node<T>* node);//checked
		Node<T>* 	getNext(void);//checked
		Node<T>*	getLast(void);//checked
	};
	
	template <class T>
	class	LinkedList{
	private:
		Node<T>		*begin,*end;
		llsize_t		len;
		/// Find a node in a given index
	//public:
		Node<T>*	find(llsize_t index);//checked
	public:
		LinkedList( void );//checked
		~LinkedList( void );//checked
		llsize_t	size(void);//checked
		llsize_t	length(void);//checked
		Node<T>*	getBeginNode(void);//checked
		Node<T>*	getEndNode(void);//checked
		llsize_t	get( T* data, llsize_t index);//checked
		llsize_t	pop( T* data, llsize_t index=0);//checked
		llsize_t	append( T data );//checked
		llsize_t	push( T data );//checked
		llsize_t	remove(llsize_t index);//checked
	};
};	
#endif
