#include <cstdlib>
#include <cstring>
#include "LinkedList.hpp"

using namespace compsci;

//T	data;
//Node*	next,last;
template	<class T>
Node<T>::Node(void){
	/// Inicializar los punteros
	this->data = 0x00;
	this->next = this->last = 0x00;
}

template	<class T>
Node<T>::~Node(void){
	/// Si se ha reservado memoria se debe liberar
	if( this->data ){
		delete this->data;
	}
}

template	<class T>
T Node<T>::get(void){
	/// Crear una copia de los datos del objeto
	T	a=0;
	if( this->data ){
		a = *this->data;
	}	
	return a;
}

template	<class T>
void Node<T>::set(T data){
	/// Crear una copia de los datos dentro del objeto
	if( !this->data ){	
		this->data = new T; 
	}
	*this->data = data;
}

template	<class T>
Node<T>* Node<T>::setNext(Node<T>* node){
	/// Copiar la referencia anterior
	Node<T>	*temp = this->next;
	this->next = node;
	return temp;
}

template	<class T>
Node<T>* Node<T>::setLast(Node<T>* node){
	Node<T> *temp = this->last;
	this->last = node;
	return temp;
}
template	<class T>
Node<T>* Node<T>::getNext(void){
	return this->next;	
}

template	<class T>
Node<T>* Node<T>::getLast(void){
	return this->last;	
}

//llsize_t	len;
//Node	*begin,*end;
template	<class T>
LinkedList<T>::LinkedList( void ){
	this->len = 0x00;
	this->begin = this->end = 0x00;
}
template	<class T>
LinkedList<T>::~LinkedList( void ){
	Node<T>	*temp = this->begin;
	while( temp ){
		this->begin = this->begin->getNext();
		delete temp;
		temp = this->begin;
	}
}
//#include <cstdio>
//#include <cstdlib>
template	<class T>
Node<T>*	LinkedList<T>::find(llsize_t index){
///	Safe index Validation{
	if( index >= 0 )
		{if( index >= this->len ){return 0x00;}}
	else
		{if( -1*index > this->len ){return 0x00;}}
///	}
	/// Compute absolute index
	if( index < 0 ){index = this->len + index;}
	/// Decide incremental or decremental search
	Node<T>*	temp;
	if( index >= (llsize_t)(this->len/2) ){	
		////////////////////////
		/// Decremental search
		////////////////////////
		
		///	Obtaining relative index
		index = this->len-index-1;
		temp = this->end;
		while(index)
		{
			temp = temp->getLast();
			index--;
		}
	}
	else
	{
		////////////////////////
		/// Incremental search
		////////////////////////
		temp = this->begin;
		while(index)
		{
			temp = temp->getNext();
			index--;
		}
	}
	return temp;
}
template	<class T>
llsize_t	LinkedList<T>::size(void){
	return this->len;
}
template	<class T>
llsize_t	LinkedList<T>::length(void){
	return this->len;
}
template	<class T>
Node<T>* LinkedList<T>::getBeginNode(void){
	return this->begin;
}
template	<class T>
Node<T>* LinkedList<T>::getEndNode(void){
	return this->end;
}
template	<class T>
llsize_t	LinkedList<T>::get(T* data, llsize_t index){
	Node<T>		*temp = this->find( index );
	if( !temp ){
		return -1;
	}
	*data = temp->get();
	return this->len;
}
template	<class T>
llsize_t	LinkedList<T>::pop(T* data, llsize_t index=0){
	if( !data ){ return -1; }
	Node<T>		*temp = this->find( index );
	if( !temp ){
		return -1;
	}
	///	Is an inner node
	if( temp->getNext() && temp->getLast() ){
		Node<T> *next,*last;
		next = temp->getNext();
		last = temp->getLast();
		next->setLast( last );
		last->setNext( next );
	}else
	///	Is the first node 
	if( temp->getNext() ){
		this->begin = this->begin->getNext();
		this->begin->setLast( 0x00 );
	}else
	///	Is the last node
	if( temp->getLast() ){
		this->end = this->end->getLast();
		this->end->setNext( 0x00 );
	}else
	///	Is the only node
	{
		this->begin = this->end = 0x00;
	}
	this->len--;
	*data = temp->get();
	delete temp;
	return this->len;
}
template	<class T>
llsize_t	LinkedList<T>::append( T data ){
	Node<T>		*temp = new Node<T>;
	temp->set(data);	
	if( this->end ){
		/// This list is not empty
		this->end->setNext( temp );
		temp->setLast( this->end );
		this->end = temp;
	}else{
		/// This list is empty
		this->end = this->begin = temp;
	}
	this->len++;
	return this->len;
}
template	<class T>
llsize_t	LinkedList<T>::push( T data ){
	Node<T>		*temp = new Node<T>;
	temp->set(data);	
	if( this->begin ){
		/// This list is not empty
		this->begin->setLast( temp );
		temp->setNext( this->begin );
		this->begin = temp;
	}else{
		/// This list is empty
		this->end = this->begin = temp;
	}
	this->len++;
	return this->len;
}
template	<class T>
llsize_t	LinkedList<T>::remove(llsize_t index){
	Node<T>		*temp = this->find( index );
	if( !temp ){
		return -1;
	}
	///	Is an inner node
	if( temp->getNext() && temp->getLast() ){
		Node<T> *next,*last;
		next = temp->getNext();
		last = temp->getLast();
		next->setLast( last );
		last->setNext( next );
	}else
	///	Is the first node 
	if( temp->getNext() ){
		this->begin = this->begin->getNext();
		this->begin->setLast( 0x00 );
	}else
	///	Is the last node
	if( temp->getLast() ){
		this->end = this->end->getLast();
		this->end->setNext( 0x00 );
	}else
	///	Is the only node
	{
		this->begin = this->end = 0x00;
	}
	this->len--;	
	delete temp;
	return this->len;
}

// Explicit instantiation
template class Node<float>;
template class Node<int>;
template class Node<char>;
template class LinkedList<float>;
template class LinkedList<int>;
template class LinkedList<char>;
