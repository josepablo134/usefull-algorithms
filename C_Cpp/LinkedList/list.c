#include "List.h"
    /*
     * Must be called before a list is used!  This initialises all the members
     * of the list structure and inserts the xListEnd item into the list as a
     * marker to the back of the list.
     *
     * @param pxList Pointer to the list being initialised.
     *
     */
void vListInitialise( List_t* pxList ){
    if(pxList){
        pxList->pvLast = pxList->pvRoot = 0x00;
        pxList->size   = 0x00;
    }
}
    /*
     * Must be called before a list item is used.  This sets the list container to
     * null so the item does not think that it is already contained in a list.
     *
     * @param pxItem Pointer to the list item being initialised.
     *
     */
void vListInitialiseItem( ListItem_t* pxItem ){
    if(!pxItem){return;}
    pxItem->pvData = 0x00;
    pxItem->pvLast = 0x00;
    pxItem->pvNext = 0x00;
    pxItem->pvList = 0x00;
}
    /*
     * Insert a list item at the beginning of a list.
     *
     * @param pxList The list into which the item is to be inserted.
     *
     * @param pxNewListItem The item that is to be placed in the list.
     *
     */
UBaseType_t uxListInsert( List_t* pxList, ListItem_t* pxNewListItem ){
    if( !pxList ){return 0x00;}
    if( !pxNewListItem ){return 0x00;}
    if( pxList->size ){
        /// El nuevo elemento apunta a ROOT como su SIGUIENTE
        pxNewListItem->pvNext = pxList->pvRoot;
        /// El nuevo elemento apunta al NULO elemento como su PREVIO
        pxNewListItem->pvLast = 0x00;
        /// El primer elemento apunta al nuevo elemento como su PREVIO
        ((ListItem_Handle)(pxList->pvRoot))->pvLast = pxNewListItem;
        /// La lista asigna la calidad de PRIMERO/RAIZ al nuevo elemento
        pxList->pvRoot = (void*) pxNewListItem;
        /// La lista crece un elemento
        pxList->size += 1;
    }else{
        /// CUANDO LA LISTA ESTA VACIA
        /// EL ULTIMO ELEMENTO ES EL PRIMERO
        /// Y LOS EXTREMOS APUNTAN NULO
        /// YA QUE NO HAY NI SIGUIENTE NI ANTERIOR
        pxList->pvRoot = (void*)pxNewListItem;
        pxList->pvLast = (void*)pxNewListItem;
        pxNewListItem->pvLast = pxNewListItem->pvNext = 0x00;
        pxList->size += 1;
    }
    /// En cualquiera de los casos, el nodo apunta a la lista
    pxNewListItem->pvList = (void*)pxList;

    return pxList->size;
}
    /*
     * Insert a list item at the end of a list.
     *
     * @param pxList The list into which the item is to be inserted.
     *
     * @param pxNewListItem The list item to be inserted into the list.
     *
     */
UBaseType_t uxListInsertEnd( List_t* pxList, ListItem_t* pxNewListItem ){
    if( !pxList ){return 0x00;}
    if( !pxNewListItem ){return 0x00;}
    if( pxList->size ){
        /// El nuevo elemento apunta a NULO como su SIGUIENTE
        pxNewListItem->pvNext = 0x00;
        /// El nuevo elemento apunta al ultimo elemento como su PREVIO
        pxNewListItem->pvLast = pxList->pvLast;
        /// El ultimo elemento apunta al nuevo elemento como su SIGUIENTE
        ((ListItem_Handle)(pxList->pvLast))->pvNext = pxNewListItem;
        /// La lista asigna la calidad de ULTIMO al nuevo elemento
        pxList->pvLast = (void*) pxNewListItem;
        /// La lista crece un elemento
        pxList->size += 1;
    }else{
        /// CUANDO LA LISTA ESTA VACIA
        /// EL ULTIMO ELEMENTO ES EL PRIMERO
        /// Y LOS EXTREMOS APUNTAN NULO
        /// YA QUE NO HAY NI SIGUIENTE NI ANTERIOR
        pxList->pvRoot = (void*)pxNewListItem;
        pxList->pvLast = (void*)pxNewListItem;
        pxNewListItem->pvLast = pxNewListItem->pvNext = 0x00;
        pxList->size += 1;
    }
    /// En cualquiera de los casos, el nodo apunta a la lista
    pxNewListItem->pvList = (void*)pxList;

    return pxList->size;
}
    /*
     * Remove an item from a list.
     *
     * @param uxListRemove The item to be removed.  The item will remove itself from
     * the list pointed to by it's pxContainer parameter.
     *
     * @return The number of items that remain in the list after the list item has
     * been removed.
     */
UBaseType_t uxListRemove( ListItem_t* pxItemToRemove ){
    register UBaseType_t    left;
    /// EL nodo no existe
    if( !pxItemToRemove ){ return 0x00; }
    /// El nodo no ha sido asignado a alguna lista 
    if( !pxItemToRemove->pvList ){ return 0x00; }
    
    /// Si el nodo es el unico en la lista
    if( !pxItemToRemove->pvLast && !pxItemToRemove->pvNext ){
        /// Desligamos la lista del nodo
        ((List_Handle)pxItemToRemove->pvList)->pvRoot = 0x00;
        ((List_Handle)pxItemToRemove->pvList)->pvLast = 0x00;
    }
    /// Si el nodo es Root
    else if( !pxItemToRemove->pvLast ){
        /// EL nodo SIGUIENTE al nodo ROOT es ahora el nodo ROOT
        ((List_Handle)pxItemToRemove->pvList)->pvRoot = pxItemToRemove->pvNext;
        /// Este nuevo nodo ROOT apunta a NULO como su PREVIO
        ((ListItem_Handle)pxItemToRemove->pvNext)->pvLast = 0x00;
    }
    /// Si el nodo es LAST
    else if( !pxItemToRemove->pvNext  ){
        /// EL nodo ANTERIOR al nodo LAST es ahora el nodo LAST
        ((List_Handle)pxItemToRemove->pvList)->pvLast = pxItemToRemove->pvLast;
        /// Este nuevo nodo LAST apunta a NULO como su SIGUIENTE
        ((ListItem_Handle)pxItemToRemove->pvLast)->pvNext = 0x00;
    }
    /// Si el nodo es un nodo INTERMEDIO
    else{
        /// El nodo PREVIO apunta al nodo SIGUIENTE
        ((ListItem_Handle)pxItemToRemove->pvLast)->pvNext = (void*)((ListItem_Handle)pxItemToRemove->pvNext);
        /// El nodo SIGUIENTE apunta al nodo PREVIO
        ((ListItem_Handle)pxItemToRemove->pvNext)->pvLast = (void*)((ListItem_Handle)pxItemToRemove->pvLast);
    }
    /// En cualquiera de los casos, La lista decrementa sus nodos en uno
    ((List_Handle)pxItemToRemove->pvList)->size -= 1;
    left = ((List_Handle)pxItemToRemove->pvList)->size;
    /// Y desligamos al nodo de cualquier contexto
    pxItemToRemove->pvLast = 0x00;
    pxItemToRemove->pvNext = 0x00;
    pxItemToRemove->pvList = 0x00;
    
    return left;
}
/*
 * Reeplace an item from a list.
 *
 * @param pxItemToRemove The item to be removed.  The item will remove itself from
 * the list pointed to by it's pxContainer parameter.
 *
 * @param pxItemToInsert The item to be inserted.
 *
 * @return The number of items that remain in the list after the list item has
 * been removed.
 */
UBaseType_t uxListReplace( ListItem_t* pxItemToRemove , ListItem_t* pxItemToInsert ){
    if( !pxItemToRemove || !pxItemToInsert){ return 0x00; }
    if( !pxItemToRemove->pvList ){ return 0x00; }

    /// Es el unico nodo
    if( !pxItemToRemove->pvNext && !pxItemToRemove->pvLast){
        pxItemToInsert->pvNext = 0x00;
        pxItemToInsert->pvLast = 0x00;
        pxItemToInsert->pvList = pxItemToRemove->pvList;
        ((List_t*) pxItemToInsert->pvList )->pvRoot = pxItemToInsert;
        ((List_t*) pxItemToInsert->pvList )->pvLast = pxItemToInsert;
    }else
    /// Es el nodo LAST
    if( !pxItemToRemove->pvNext ){
        pxItemToInsert->pvNext = 0x00;
        pxItemToInsert->pvLast = pxItemToRemove->pvLast;
        ((ListItem_t*)pxItemToRemove->pvLast)->pvNext = pxItemToInsert;
        pxItemToInsert->pvList = pxItemToRemove->pvList;
        ((List_t*) pxItemToInsert->pvList )->pvLast = pxItemToInsert;

    }else
    /// Es el nodo ROOT
    if( !pxItemToRemove->pvLast){
        pxItemToInsert->pvNext = pxItemToRemove->pvNext;
        pxItemToInsert->pvLast = 0x00;
        ((ListItem_t*)pxItemToRemove->pvNext)->pvLast = pxItemToInsert;
        pxItemToInsert->pvList = pxItemToRemove->pvList;
        ((List_t*) pxItemToInsert->pvList )->pvRoot = pxItemToInsert;
    }
    /// Es un nodo INTERMEDIO
    else{
        pxItemToInsert->pvNext = pxItemToRemove->pvNext;
        pxItemToInsert->pvLast = pxItemToRemove->pvLast;
        pxItemToInsert->pvList = pxItemToRemove->pvList;
        ((ListItem_t*)pxItemToRemove->pvLast)->pvNext = pxItemToInsert;
        ((ListItem_t*)pxItemToRemove->pvNext)->pvLast = pxItemToInsert;
    }
    pxItemToRemove->pvNext = 0x00;
    pxItemToRemove->pvLast = 0x00;
    pxItemToRemove->pvList = 0x00;
    return ((List_t*)pxItemToInsert->pvList)->size;
}

/*
 * Insert an item in a list after an defined item.
 *
 * @param pxItem The reference item.
 *
 * @param pxItemToInsert The item to insert.
 *
 */
UBaseType_t uxListInsertAfter( ListItem_t* pxItem, ListItem_t* pxItemToInsert ){
    if( !pxItem || !pxItemToInsert){ return 0x00; }
    if( !(pxItem->pvList) ){ return 0x00; }

        /// Root item
    if( !pxItem->pvNext && !pxItem->pvLast ){
        /// Enlazar nuevo item al actual
        pxItem->pvNext = pxItemToInsert;
        pxItemToInsert->pvNext = 0x00;
        pxItemToInsert->pvLast = pxItem;
        pxItemToInsert->pvList = pxItem->pvList;
        /// Enlazar nuevo item a la lista
        ((List_t*)pxItem->pvList)->pvLast = pxItemToInsert;
    }else
        /// Last Item
    if( !pxItem->pvNext ){
        /// Enlazar nuevo item al actual
        pxItem->pvNext = pxItemToInsert;
        pxItemToInsert->pvNext = 0x00;
        pxItemToInsert->pvLast = pxItem;
        pxItemToInsert->pvList = pxItem->pvList;
        /// Enlazar nuevo item a la lista
        ((List_t*)pxItem->pvList)->pvLast = pxItemToInsert;
    }else
        /// First Item
    if( !pxItem->pvLast ){
        /// Enlazar nuevo item al actual
        pxItemToInsert->pvNext = pxItem->pvNext;
        pxItemToInsert->pvLast = pxItem;
        pxItemToInsert->pvList = pxItem->pvList;
        ((ListItem_t*)pxItem->pvNext)->pvLast = pxItemToInsert;
        pxItem->pvNext = pxItemToInsert;
        /// No es necesario manipular los extremos de la lista
    }
        /// Inner Item
    else{
        /// Enlazar nuevo item al actual
        pxItemToInsert->pvNext = pxItem->pvNext;
        pxItemToInsert->pvLast = pxItem;
        pxItemToInsert->pvList = pxItem->pvList;
        ((ListItem_t*)pxItem->pvNext)->pvLast = pxItemToInsert;
        pxItem->pvNext = pxItemToInsert;
        /// No es necesario manipular los extremos de la lista
    }

    /// Incrementar en uno los elementos de la lista
    ((List_t*)pxItem->pvList)->size += 1;
    return ((List_t*)pxItem->pvList)->size;
}

/*
 * Remove an item from a list.
 *
 * @param uxListRemove The item to be removed.  The item will remove itself from
 * the list pointed to by it's pxContainer parameter.
 *
 * @return The number of items that remain in the list after the list item has
 * been removed.
 */
UBaseType_t uxListInsertBefore( ListItem_t* pxItem, ListItem_t* pxItemToInsert ){
    if( !pxItem || !pxItemToInsert){ return 0x00; }
    if( !(pxItem->pvList) ){ return 0x00; }

        /// Root item
    if( !pxItem->pvNext && !pxItem->pvLast ){
        /// Enlazar nuevo item al actual
        pxItemToInsert->pvNext = pxItem;
        pxItemToInsert->pvLast = 0x00;
        pxItemToInsert->pvList = pxItem->pvList;
        pxItem->pvLast = pxItemToInsert;
        /// Enlazar nuevo item a la lista
        ((List_t*)pxItem->pvList)->pvRoot = pxItemToInsert;
    }else
        /// Last Item
    if( !pxItem->pvNext ){
        /// Enlazar nuevo item al actual
        pxItemToInsert->pvLast = pxItem->pvLast;
        pxItemToInsert->pvList = pxItem->pvList;
        pxItemToInsert->pvNext = pxItem;
        ((ListItem_t*)pxItem->pvLast)->pvNext = pxItemToInsert;
        pxItem->pvLast = pxItemToInsert;
        /// No es necesario manipular los extremos de la lista
    }else
        /// First Item
    if( !pxItem->pvLast ){
        /// Enlazar nuevo item al actual
        pxItemToInsert->pvNext = pxItem;
        pxItemToInsert->pvLast = 0x00;
        pxItemToInsert->pvList = pxItem->pvList;
        pxItem->pvLast = pxItemToInsert;
        /// Enlazar nuevo item a la lista
        ((List_t*)pxItem->pvList)->pvRoot = pxItemToInsert;
    }
        /// Inner Item
    else{
        /// Enlazar nuevo item al actual
        pxItemToInsert->pvLast = pxItem->pvLast;
        pxItemToInsert->pvList = pxItem->pvList;
        pxItemToInsert->pvNext = pxItem;
        ((ListItem_t*)pxItem->pvLast)->pvNext = pxItemToInsert;
        pxItem->pvLast = pxItemToInsert;
        /// No es necesario manipular los extremos de la lista
    }

    /// Incrementar en uno los elementos de la lista
    ((List_t*)pxItem->pvList)->size += 1;
    return ((List_t*)pxItem->pvList)->size;
}
