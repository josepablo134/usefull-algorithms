#include "uMalloc.h"
#include "stdio.h"

extern size_t um_freeMem();
extern size_t um_usedMem();

typedef long double var;
int main(int argc , char** argv){
	var	*a,*b;
	a = (var*) um_malloc( sizeof(var) );
	b = (var*) um_malloc( sizeof(var) );
	*a = 3.1416;
	*b = (*a)*2;
	printf("%.3Lf\n", (*b)/(*a) );
	printf("Heap memory : %d bytes\n", uMALLOC_HEAP_SIZE );
	printf("Free memory : %ld bytes\n", um_freeMem() );
	printf("Used memory : %ld bytes\n", um_usedMem() );

	um_free(a);
	um_free(b);
	return 0;
}
