#ifndef uMALLOC_H_
#define uMALLOC_H_
	#include <stdlib.h>
	#define			uMALLOC_HEAP_SIZE	1024//bytes
	void*	um_malloc( size_t );
	void	um_free( void* );
#endif
