#include "uMalloc.h"
static unsigned char	_uHEAP_[ uMALLOC_HEAP_SIZE ];
size_t					um_ptr=0;

void* um_malloc( size_t len ){
	um_ptr += len;
	if( um_ptr >= uMALLOC_HEAP_SIZE ){
		um_ptr -= len;
		return 0x00;
	}
	return &_uHEAP_[ um_ptr ];
}
void um_free( void* buffer ){
	buffer = 0x00;
	return;
}

size_t	um_freeMem(){
	return uMALLOC_HEAP_SIZE - um_ptr;
}
size_t um_usedMem(){
	return um_ptr;
}
