#include "PIDSystem.h"
/*
		var Kp,Ki,Kd;
		var	Ts;
		var	error;
		var delta_error;
		var i_error;
*/
void PIDSystemInit( PIDSystem_t* pid ){
	if(!pid){ return; }
	pid->Kp = pid->Ki = pid->Kd = 0.0;
	pid->Ts = \
	pid->error = \
	pid->delta_error = \
	pid->i_error = 0.0;
}
var PIDSystemCompute( var input , var setpoint , PIDSystem_t* pid ){
	var output;
	var error;
	error = setpoint - input;
	/// Compute control
	output = pid->Kp*error;
			///	Derivative of error
			pid->delta_error = error - pid->error;
			pid->delta_error /= pid->Ts;
			///	Integral of error
			pid->i_error += error*pid->Ts;
			/// Save state in memory
			pid->error = error;
			///	Evaluate control compensator
			output += pid->Kd * pid->delta_error;
			output += pid->Ki * pid->i_error;
	return output;
}
