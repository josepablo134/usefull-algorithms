#ifndef PIDSYSTEM_H_
#define PIDSYSTEM_H_
	typedef float var;
	typedef struct PIDState_t{
		var Kp,Ki,Kd;
		var	Ts;
		var	error;
		var delta_error;
		var i_error;
	}PIDState_t;
	void PIDSystemInit( PIDSystem_t* );
	var PIDSystemCompute( var , var , PIDSystem_t* );
#endif
