/*!
*	@file		LTISystem.c
*	@version	1.0
*	@author		Josepablo Cruz Baas
*	@date		04-29-2019
*	@title		LTISystem ( Linear Time-Invariant System ) Implementation
*	Example of use:
*	@code
*		/// PSEUDO CODE FOR A SENSOR FILTERING SYSTEM
*		const size_t	filterOrder = N;
*		const var		a[ N+1 ] = { ... };
*		const var		b[ N+1 ] = { ... };
*		static var		xdelay[ N ];
*		static var		ydelay[ N ];
*		LTITransfer_t	filter;
*		var				dataIn,dataOut;
*		LTITransferInit( &filter , a , b , xdelay , ydelay , N , N );
*		while( true ){
*			dataIn = sensorRead(  );
*			dataOut = LTITransferCompute( dataIn , &filter );
*			delay( x );
*		}
*	@endcode
**/
#include "LTISystem.h"

	/*!
	*	@sa	LTITransferInit( value , vector , len )
	*	@brief	Initialize the transfer function object
	*	@param	system, ptr to LTITransfer_t structure
	*	@param	a , Denominator terms of the transfer function
	*	@param	b , Numerator terms of the transfer function
	*	@param	xdelay , Delay vector of the signal x
	*	@param	ydelay , Delay vector of the signal y
	*	@param	N , Order of the Denoominator
	*	@param	M , Order of the Numerator
	**/
void LTITransferInit( LTITransfer_t* system ,
                               const var* a,
                               const var* b,
                               var* xdelay,
                               var* ydelay,
                               uint32_t N,
                               uint32_t M ){
    register uint32_t   k;
    if( a && b && xdelay && ydelay ){
        system->M = M;// X - order
        system->N = N;// Y - order
        system->a = a;
        system->b = b;
        system->xdelay = xdelay;
        system->ydelay = ydelay;
        /// Clear intial conditions
        for( k=0; k<M ; k++ ){
            system->xdelay[ k ] = \
            system->ydelay[ k ] = 0.0;
        }
        for(; k<N ; k++ ){
            system->xdelay[ k ] = 0.0;
        }
        return;
    }
    system->M = M;
    system->N = N;
    system->a = \
    system->b = \
    system->xdelay = \
    system->ydelay = 0x00;
}
	/*!
	*	@sa	LTITransferCompute( value , vector , len )
	*	@brief	Compute a value in the transfer function.
	*	@param	value , a var type (BLAS.h) value to compute
	*	@param	system , a LTITransfer_t ptr, a transfer function structure
	*	@return result , a var type (BLAS.h) result of the computation
	**/
var  LTITransferCompute( var input, LTITransfer_t* system){
    register uint32_t   k;
    var                 tempOut = input*system->b[0];
    for(k=0; k < system->M ; k++){
        tempOut += system->xdelay[ k ] * system->b[ k+1 ];
        tempOut -= system->ydelay[ k ] * system->a[ k+1 ];
    }
    for(; k < system->N ; k++){
        tempOut += system->xdelay[ k ] * system->b[ k+1 ];
    }
    tempOut /= system->a[0];
    for( k=system->N-1 ; k >= system->M ; k-- ){
        system->xdelay[ k ] = system->xdelay[ k-1 ];
    }
    for( ; k>0 ; k-- ){
        system->xdelay[ k ] = system->xdelay[ k-1 ];
        system->ydelay[ k ] = system->ydelay[ k-1 ];
    }
    system->xdelay[ 0 ] = input;
    system->ydelay[ 0 ] = tempOut;
    return tempOut;
}
