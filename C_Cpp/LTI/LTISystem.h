/*!
*	@file		LTISystem.h
*	@version	1.0
*	@author		Josepablo Cruz Baas
*	@date		04-29-2019
*	@title		LTISystem ( Linear Time-Invariant System )
*	Example of use:
*	@code
*		/// PSEUDO CODE FOR A SENSOR FILTERING SYSTEM
*		const size_t	filterOrder = N;
*		const var		a[ N+1 ] = { ... };
*		const var		b[ N+1 ] = { ... };
*		static var		xdelay[ N ];
*		static var		ydelay[ N ];
*		LTITransfer_t	filter;
*		var				dataIn,dataOut;
*		LTITransferInit( &filter , a , b , xdelay , ydelay , N , N );
*		while( true ){
*			dataIn = sensorRead(  );
*			dataOut = LTITransferCompute( dataIn , &filter );
*			delay( x );
*		}
*	@endcode
**/
#ifndef SYSTEMSTATE_H_
#define SYSTEMSTATE_H_
    #include <stdint.h>
	#include "../BLAS/BLAS.h"
	typedef blas_var	var;
	typedef struct LTITransfer_t{
		size_t		N,M;				/// DenOrder, NumOrder
		const var   *a,*b;				/// Den, Numerator
		var         *xdelay,*ydelay;	///	delay vectors
	}LTITransfer_t;
	/*!
	*	@sa	LTITransferInit( value , vector , len )
	*	@brief	Initialize the transfer function object
	*	@param	system, ptr to LTITransfer_t structure
	*	@param	a , Denominator terms of the transfer function
	*	@param	b , Numerator terms of the transfer function
	*	@param	xdelay , Delay vector of the signal x
	*	@param	ydelay , Delay vector of the signal y
	*	@param	N , Order of the Denoominator
	*	@param	M , Order of the Numerator
	**/
	extern void LTITransferInit( LTITransfer_t* system,
										const var* a,
										const var* b,
										var* xdelay,
										var* ydelay,
										uint32_t N,
										uint32_t M );
	/*!
	*	@sa	LTITransferCompute( value , vector , len )
	*	@brief	Compute a value in the transfer function.
	*	@param	input , a var type (BLAS.h) value to compute
	*	@param	system , a LTITransfer_t ptr, a transfer function structure
	*	@return output , a var type (BLAS.h) result of the computation
	**/
    extern var  LTITransferCompute( var input , LTITransfer_t* system );
#endif
