#! /usr/bin/python3
import pandas
import sys
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as signal
from LTISystem import LTISystemTransfer as LTISystem

if( len(sys.argv) < 5 ):
	print( sys.argv[0] , "[csv file] [sample rate] [cut freq] [N filter order]")
	print("Nyquist Freq 200Hz " )
	exit(-1)

inputFile = sys.argv[1]
samplerate = int( sys.argv[2] )
fc = int(sys.argv[3])
FilterOrder = int(sys.argv[4])

data = pandas.read_csv( inputFile )
N = len(data['data'])

plt.ion()
plt.figure(1)
plt.plot( data['data'] )
plt.show()

Wn = fc/samplerate
(b,a) = signal.butter( FilterOrder , Wn , 'low' , False , 'ba' , None )

Xlpf = LTISystem( b , a )

x = np.zeros( N )

for k in range( N ):
	x[k] = Xlpf.compute( data['data'][k] )

plt.ion()
plt.figure(2)
plt.plot( x )
plt.show()

print("LPF parameters")
print( "Num : " , Xlpf.b )
print( "Den : " , Xlpf.a )

input()
