import numpy as np
class LTISystemTransfer:
    def __init__( self , b, a ):
        self.b = b #Numerador
        self.a = a #Denominador
        self.N = len( b )-1
        self.M = len( a )-1
        self.xdelay = np.zeros( self.N )
        self.ydelay = np.zeros( self.M )
    def compute( self , i ):
        # Compute the actual output
        y = i * self.b[0]
        for k in range( self.N ):
            y += self.b[ k+1 ] * self.xdelay[ k ]
            if( (k+1) <= self.M ):
                y -= self.a[ k+1 ]*self.ydelay[ k ]
        y /= self.a[ 0 ]
        k = self.N-1
        while( k ):
            self.xdelay[ k ] = self.xdelay[ k-1 ]
            if( k<self.M ):
                self.ydelay[ k ] = self.ydelay[ k-1 ]
            k -= 1
        self.xdelay[ 0 ] = i
        self.ydelay[ 0 ] = y
        return y
'''
float	dataFilter( DataState* state, float input ){
	register int 	k;
	float			output= input * state->b[0];
	for(k=0 ; k<state->n ; k++){
		output += state->b[ k+1 ] * state->xdelay[ k ];
		if( k+1 <= state->m ){
			output -= state->a[ k+1 ]*state->ydelay[ k ];
		}
	}
	output /= state->a[ 0 ];
	for(k=state->n-1 ; k>0 ; k--){
		state->xdelay[ k ] = state->xdelay[ k-1 ];
		if( k < state->m  ){
			state->ydelay[ k ] = state->ydelay[ k-1 ];
		}
	}
	state->xdelay[ k ] = input;
	state->ydelay[ k ] = output;
	return output;
}
'''
