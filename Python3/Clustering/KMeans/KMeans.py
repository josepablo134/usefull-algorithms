from numpy import *
from numpy.random import rand

##########################################
#	Linear distance estimation
#
def vectorDistance( x , y ):
	return sqrt( sum( (y-x)**2 ) ) 

##########################################
#	K-Means segmentation
#
'''
	Let n be the number of classes and m the dimension of one element.
	This algorithm randomly generates n vectors of dimension m called centers,
	then for each element in the input, the distance to every center is computed
	and compared, the elements are labeled depending on the distance. 
	Once all the elements have been computed, the centers are reorganiced
	based on the mean of all labeled elements on its class and the algorithms restart.
	This loop runs until the centers change rate gets lower than the threshold
	or the number of epochs is reach, any of the events. 
	Is possible to disable a event setting the respective param to None
	
	@param	elements: A matrix of x m dimension vectors, each one a element to classifiy
	@param	classes: Integer, the number of classes 
	@param	distanceFxn: Distance computer, a callback to be called for each distance calculation
	@param	threshold: Centers change rate threshold to stop iteration. 0.0 means, when the centers stop moving.
	@param	epochs: Integer, maximum number of iterations. None means the limit does not exists.
'''
def KMeans( elements , classes , distanceFxn=vectorDistance , threshold=0.1 , epochs=None ):
	dimension = elements.shape[1]

	center = rand( classes , dimension  )
	center_mean = zeros( center.shape )
	center_sum  = zeros( center.shape[0] )
	last_center = center.copy()
	center_change_rate = 1.0

	distance = zeros( center.shape[0] )
	
	if( epochs != None ):
		for k in range(epochs):
			# Para cada elemento
			for i in range( elements.shape[0] ):
				# hacia cada centro
				for j in range( center.shape[0] ):
					# Evaluar la distancia
					distance[j] = distanceFxn( elements[i] , center[j] )
				# Encontrar el minimo y etiquetar
				toTag = argmin( distance )
				center_mean[ toTag ] += elements[i]
				center_sum[ toTag ] += 1
			# Calculate the new center
			for i in range( center.shape[0] ):
				for j in range( center.shape[1] ):
					if( center_sum[i] != 0.0 ):
							center[i][j] = center_mean[i][j] / center_sum[i]
			if( threshold != None ):
				#center_change_rate = mean( center - last_center )
				center_change_rate = distanceFxn( last_center , center )
				if( center_change_rate <= threshold ):
					break
			# Preparing for the next iteration
			last_center = center.copy()
			center_mean *= 0.0 
			center_sum *= 0.0
	else:
		while(1):
			# Para cada elemento
			for i in range( elements.shape[0] ):
				# hacia cada centro
				for j in range( center.shape[0] ):
					# Evaluar la distancia
					distance[j] = distanceFxn( elements[i] , center[j] )
				# Encontrar el minimo y etiquetar
				toTag = argmin( distance )
				center_mean[ toTag ] += elements[i]
				center_sum[ toTag ] += 1
			# Calculate the new center
			for i in range( center.shape[0] ):
				for j in range( center.shape[1] ):
					if( center_sum[i] != 0.0 ):
							center[i][j] = center_mean[i][j] / center_sum[i]
			if( threshold != None ):
				#center_change_rate = mean( center - last_center )
				center_change_rate = distanceFxn( last_center , center )
				if( center_change_rate <= threshold ):
					break
			# Preparing for the next iteration
			last_center = center.copy()
			center_mean *= 0.0 
			center_sum *= 0.0
	# Get the clusters
	cluster = []
	for k in range( classes ):
		cluster.append([])
	for i in range( elements.shape[0] ):
		for j in range( center.shape[0] ):
			distance[j] = distanceFxn( elements[i] , center[j] )
		toTag = argmin( distance )
		cluster[toTag].append( elements[i] )
	# Convert to array of matrices
	for k in range( classes ):
		cluster[k] = array(cluster[k])
	return array(cluster),center

