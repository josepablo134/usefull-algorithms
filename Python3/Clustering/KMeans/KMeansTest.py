from KMeans import *
from numpy import *
from numpy.random import rand
from matplotlib.pyplot import *
from mpl_toolkits.mplot3d import Axes3D
import time
ion()
fig = figure()
ax = fig.add_subplot(111, projection='3d')

training = rand( 200 , 3 )*1000.0
ax.scatter( training.T[0] , training.T[1], training.T[2] , label="training data" ) 
legend()
title("Input")
show()

print( "Training data sets len %d"%len(training) )
print("Press enter to continue...")
input()

t1 = time.time()
(clusters , centroids ) = KMeans( training , 7 , threshold=0.0 , epochs = 100 )
t2 = time.time()
dt = t2-t1
print( "Execution time %f"%(dt) )


fig = figure(2)
ax = fig.add_subplot(111,projection='3d')
for k in range( clusters.shape[0] ):
	ax.scatter( clusters[k].T[0] , clusters[k].T[1] , clusters[k].T[2] , label="cluster %d"%(k+1) )
for k in range( centroids.shape[0] ):
	print( "Center %d : "%(k+1) , centroids[k] )
	ax.scatter( centroids[k][0] , centroids[k][1] , clusters[k].T[2] , s=200 , alpha=0.5 , label="center %d"%(k+1) )
legend()
title("Clusters")
show()

input()
