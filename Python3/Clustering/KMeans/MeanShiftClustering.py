from numpy import *
from numpy.random import rand

##########################################
#	Linear distance estimation
#
def vectorDistance( x , y ):
	return sqrt( sum( (y-x)**2 ) ) 

##########################################
#	Means-Shift Clustering
#
def MeansShift( elements , classes , distanceFxn=vectorDistance , threshold=0.1 , epochs=None ):
	dimension = elements.shape[1]

	center = rand( classes , dimension  )
	center_mean = zeros( center.shape )
	center_sum  = zeros( center.shape[0] )
	last_center = center.copy()
	center_change_rate = 1.0

	distance = zeros( center.shape[0] )
	
	if( epochs != None ):
		for k in range(epochs):
			# Para cada elemento
			for i in range( elements.shape[0] ):
				# hacia cada centro
				for j in range( center.shape[0] ):
					# Evaluar la distancia
					distance[j] = distanceFxn( elements[i] , center[j] )
				# Encontrar el minimo y etiquetar
				toTag = argmin( distance )
				center_mean[ toTag ] += elements[i]
				center_sum[ toTag ] += 1
			# Calculate the new center
			for i in range( center.shape[0] ):
				for j in range( center.shape[1] ):
					if( center_sum[i] != 0.0 ):
							center[i][j] = center_mean[i][j] / center_sum[i]
			if( threshold != None ):
				#center_change_rate = mean( center - last_center )
				center_change_rate = distanceFxn( last_center , center )
				if( center_change_rate <= threshold ):
					break
			# Preparing for the next iteration
			last_center = center.copy()
			center_mean *= 0.0 
			center_sum *= 0.0
	else:
		while(1):
			# Para cada elemento
			for i in range( elements.shape[0] ):
				# hacia cada centro
				for j in range( center.shape[0] ):
					# Evaluar la distancia
					distance[j] = distanceFxn( elements[i] , center[j] )
				# Encontrar el minimo y etiquetar
				toTag = argmin( distance )
				center_mean[ toTag ] += elements[i]
				center_sum[ toTag ] += 1
			# Calculate the new center
			for i in range( center.shape[0] ):
				for j in range( center.shape[1] ):
					if( center_sum[i] != 0.0 ):
							center[i][j] = center_mean[i][j] / center_sum[i]
			if( threshold != None ):
				#center_change_rate = mean( center - last_center )
				center_change_rate = distanceFxn( last_center , center )
				if( center_change_rate <= threshold ):
					break
			# Preparing for the next iteration
			last_center = center.copy()
			center_mean *= 0.0 
			center_sum *= 0.0
	# Get the clusters
	cluster = []
	for k in range( classes ):
		cluster.append([])
	for i in range( elements.shape[0] ):
		for j in range( center.shape[0] ):
			distance[j] = distanceFxn( elements[i] , center[j] )
		toTag = argmin( distance )
		cluster[toTag].append( elements[i] )
	# Convert to array of matrices
	for k in range( classes ):
		cluster[k] = array(cluster[k])
	return array(cluster),center

