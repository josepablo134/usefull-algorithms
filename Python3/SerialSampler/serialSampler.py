#! /usr/bin/python3
import serial
import sys
from Libs import packageReader

if( len(sys.argv) < 4 ):
	print( sys.argv[0] , "[port] [bauds] [samples]" )
	exit(-1)

s = serial.Serial()
s.port = sys.argv[1]
s.baudrate = int(sys.argv[2])
s.timeout = 1.0

samples = int(sys.argv[3])

s.open()
if(not s.is_open):
	print( sys.argv[1] , sys.argv[2] , "error opening the port")
	exit(-1)

packageReader.Flush(s) 
s.write( b'A' )
#for k in range(100):
#	packageReader.Sync( s )
#print( packageReader.Sync( s ) )
#print( packageReader.readPackage(s) )

print("rads per seconds")
while( samples ):
	data_in_bytes = packageReader.readPackage( s )
	data_in_float = packageReader.bytesToFloat( data_in_bytes )
	#print( data_in_bytes , "," , data_in_float )
	print( data_in_float )
	#print( packageReader.readPackage( s ) )
	#print( s.read(12) )
	samples -= 1
s.close()
