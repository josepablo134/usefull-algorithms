import struct
import serial

def Flush( port ):
	while( port.read() ):
		continue
def Sync( port ):
	r = port.read()
	while( r ):
		if( r == b'H' ):
			s = port.read( 11 )
			break
		r = port.read()
	return r+s
		

PACKAGE_LEN = 12
PACKAGE_HEADER = b'HEADER\x00'
PACKAGE_HEADER_LEN = 7
def readPackage( port ):
	counter = PACKAGE_LEN-1
	pack = b''
	dbyte = port.read()
	while( dbyte and counter ):
		counter -= 1
		pack += dbyte
		dbyte = port.read()
	if( pack[0:PACKAGE_HEADER_LEN] == PACKAGE_HEADER ):
		return pack[PACKAGE_HEADER_LEN:]
	return b''

def bytesToFloat( data , endianness='big' , float_type='float' ):
	if( data == b'' ):
		return -0.0
	if( endianness == 'little' ):
		if( float_type == 'float' ):
			return struct.unpack("<f",data)[0]
		elif( float_type == 'double' ):
			return struct.unpack("<d",data)[0]
	elif( endianness == 'big' ):
		if( float_type == 'float' ):
			return struct.unpack(">f",data)[0]
		elif( float_type == 'double' ):
			return struct.unpack(">d",data)[0]
	return -0.0
			
